import { Component } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslateService } from "@ngx-translate/core";
import * as firebase from "firebase/app";

@Component({
  styleUrls: ["./login.component.scss"],
  templateUrl: "./login.component.html",
})
export class LoginComponent {
  loginFormGroup = new FormGroup({
    loginEmailFormControl: new FormControl("", [
      Validators.required,
      Validators.email,
    ]),
    loginPasswordFormControl: new FormControl("", [
      Validators.required,
      Validators.minLength(6),
    ]),
  });

  registerFormGroup = new FormGroup({
    registerDisplayNameFormControl: new FormControl("", [
      Validators.required,
    ]),
    registerEmailFormControl: new FormControl("", [
      Validators.required,
      Validators.email,
    ]),
    registerPasswordFormControl: new FormControl("", [
      Validators.required,
      Validators.minLength(6),
    ]),
  });

  constructor(
    private readonly dialogRef: MatDialogRef<LoginComponent>,
    private readonly auth: AngularFireAuth,
    private readonly ts: TranslateService,
    private readonly snackBar: MatSnackBar,
  ) { }

  getError(translateKeyPrefix: string, formControl: FormControl): string {
    if (formControl.errors === null)
      return "";
    const errorCode: string = Object.keys(formControl.errors)[0];
    return this.ts.instant(translateKeyPrefix + errorCode, formControl.getError(errorCode));
  }

  get loginEmailFormControl(): FormControl {
    return this.loginFormGroup.get("loginEmailFormControl") as FormControl;
  }

  get loginPasswordFormControl(): FormControl {
    return this.loginFormGroup.get("loginPasswordFormControl") as FormControl;
  }

  get registerDisplayNameFormControl(): FormControl {
    return this.registerFormGroup.get("registerDisplayNameFormControl") as FormControl;
  }

  get registerEmailFormControl(): FormControl {
    return this.registerFormGroup.get("registerEmailFormControl") as FormControl;
  }

  get registerPasswordFormControl(): FormControl {
    return this.registerFormGroup.get("registerPasswordFormControl") as FormControl;
  }

  async login(): Promise<void> {
    try {
      await this.auth.auth.signInWithEmailAndPassword(
        this.loginEmailFormControl.value, this.loginPasswordFormControl.value);
      this.dialogRef.close(true);
    } catch (e) {
      // tslint:disable-next-line: no-console
      console.warn("login failed: ", e);
      // https://firebase.google.com/docs/reference/js/firebase.auth.Auth#signInWithEmailAndPassword
      switch (e.code) {
        case "auth/invalid-email":
          this.loginEmailFormControl.setErrors({ email: e });
          break;
        case "auth/user-disabled":
          this.loginEmailFormControl.setErrors({ "user-disabled": e });
          break;
        case "auth/user-not-found":
          this.loginEmailFormControl.setErrors({ "user-not-found": e });
          break;
        case "auth/wrong-password":
          this.loginPasswordFormControl.setErrors({ "wrong-password": e });
          break;
        default:
          this.snackBar.open(e.message || e);
          break;
      }
    }
  }

  async register(): Promise<void> {
    try {
      const userCredential: firebase.auth.UserCredential = await this.auth.auth.createUserWithEmailAndPassword(
        this.registerEmailFormControl.value, this.registerPasswordFormControl.value);
      // tslint:disable-next-line:no-non-null-assertion
      await userCredential.user!.updateProfile({
        displayName: this.registerDisplayNameFormControl.value,
        // tslint:disable-next-line:no-null-keyword
        photoURL: null,
      });
      this.dialogRef.close(true);
    } catch (e) {
      // tslint:disable-next-line: no-console
      console.warn("register failed: ", e);
      // https://firebase.google.com/docs/reference/js/firebase.auth.Auth.html#createUserWithEmailAndPassword
      switch (e.code) {
        case "auth/email-already-in-use":
          this.registerEmailFormControl.setErrors({ "email-already-in-use": e });
          break;
        case "auth/invalid-email":
          this.registerEmailFormControl.setErrors({ email: e });
          break;
        case "auth/weak-password":
          this.registerPasswordFormControl.setErrors({ minlength: e });
          break;
        default:
          this.snackBar.open(e.message || e);
          break;
      }
    }
  }

  loginWithGoogle(): void {
    this.loginWith(new firebase.auth.GoogleAuthProvider());
  }

  loginWithGitHub(): void {
    this.loginWith(new firebase.auth.GithubAuthProvider());
  }

  loginWithFacebook(): void {
    this.loginWith(new firebase.auth.FacebookAuthProvider());
  }

  loginWithTwitter(): void {
    // https://apps.twitter.com/
    this.loginWith(new firebase.auth.TwitterAuthProvider());
  }

  private loginWith(provider: firebase.auth.AuthProvider): void {
    this.dialogRef.close(true);
    firebase
      .auth()
      .useDeviceLanguage();
    void this.auth.auth.signInWithRedirect(provider);
  }

}
