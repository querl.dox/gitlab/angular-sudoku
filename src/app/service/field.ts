import { generateOneToNine, OneToNine, OneToNineOrUndefined } from "app/service/digit";

export class Field {
  private readonly possibleDigits: boolean[] = [];
  private _digit?: OneToNine;

  constructor() {
    for (const d of generateOneToNine())
      this.possibleDigits[d] = true;
  }

  get digit(): OneToNineOrUndefined {
    return this._digit;
  }

  set digit(digit: OneToNineOrUndefined) {
    if (this._digit !== undefined)
      throw new Error("field was already set");
    if (!this.isDigitPossible(digit))
      throw new Error("digit is not possible");

    this._digit = digit;
    for (const d of generateOneToNine())
      this.crossOutDigit(d);
  }

  getOnlyPossibleDigit(): OneToNineOrUndefined {
    if (this.digit !== undefined)
      return undefined;
    let result: OneToNineOrUndefined;
    for (const digit of generateOneToNine())
      if (this.isDigitPossible(digit))
        if (result === undefined)
          result = digit;
        else
          return undefined;
    return result;
  }

  isDigitPossible(digit?: OneToNine): boolean {
    if (digit !== undefined)
      return this.possibleDigits[digit];
    return true;
  }

  crossOutDigit(digit: OneToNine): void {
    this.possibleDigits[digit] = false;
  }

}
